//
//  newSongViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/17/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit

class NewSongViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var composerTextField: UITextField!
    @IBOutlet weak var beatsPerMeasureTextField: UITextField!
    @IBOutlet weak var metronomeTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }


    @IBAction func newPageTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toCameraView" , sender: nil)
    }
    
    
}

extension NewSongViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
}
