//
//  HomeViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/17/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var songs = [
        "Rhapsody ",
        " Concerto in G Major ",
        "Moonlight Sonata",
        ]

    override func viewDidLoad() {
        super.viewDidLoad()
        

        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.isHidden = true
       
    }
    
    @IBAction func plusButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toNewSong", sender: nil)
    }
    
    
        

}
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = songs[indexPath.row]
        return cell
        
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toPlayView", sender: nil)
    }
}
