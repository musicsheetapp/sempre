//
//  PlayViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/22/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit
import AVFoundation
class PlayViewController: UIViewController {
    @IBOutlet weak var musicImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    
    var audioPlayer = AVAudioPlayer()
    
    var timer = Timer()
    var timerIsRunning = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sound = Bundle.main.path(forResource: "Click", ofType: "mp3")
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
            
        } catch{
            print(error)
        }
    

        // Do any additional setup after loading the view.
    }
    

    @IBAction func playButton(_ sender: Any) {
        if !timerIsRunning {
            //start playing
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PlayViewController.playMetSound), userInfo: nil, repeats: true)
            playButton.setTitle("Stop", for: .normal)
    
            
            
        }else{
            //stop playing
            timer.invalidate()
            playButton.setTitle("Start", for: .normal)
        }
        
        timerIsRunning = !timerIsRunning

    }
    
    @objc func playMetSound() {
        audioPlayer.play()
        
    }
    
    
    
}

